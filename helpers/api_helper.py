from .settings import URL, HEADERS
import requests


class ApiClient:
    def create_user(self, data, path="/v2/user"):
        """
        :param data:
        :param path:
        :return:
        """
        response = requests.post(url=f'{URL}{path}',
                                 headers=HEADERS,
                                 json=data)
        response.raise_for_status()
        return response

    def log_in_user(self, params, path="/v2/user/login"):
        """
        :param params:
        :param path:
        :return:
        """
        response = requests.get(url=f'{URL}{path}',
                                headers=HEADERS,
                                params=params)
        response.raise_for_status()
        return response

    def get_user_data(self, username, path="/v2/user/"):
        """
        :param username:
        :param path:
        :return:
        """
        response = requests.get(url=f'{URL}{path}{username}',
                                headers=HEADERS)
        response.raise_for_status()
        return response

    def logout_user(self, path="/v2/user/logout"):
        response = requests.get(url=f'{URL}{path}',
                                headers=HEADERS)
        response.raise_for_status()
        return response

    def delete_user(self, username, path="/v2/user/"):
        """
        :param username:
        :param path:
        :return:
        """
        response = requests.delete(url=f'{URL}{path}{username}',
                                   headers=HEADERS)
        response.raise_for_status()
        return response

    def create_pet(self, data, path="/v2/pet"):
        """
        :param data:
        :param params:
        :param path:
        :return:
        """
        response = requests.post(
            url=f'{URL}{path}',
            headers=HEADERS,
            json=data
        )
        response.raise_for_status()
        return response

    def get_by_id(self, petId, path=f"/v2/pet/"):
        """
        :param petId:
        :param path:
        :return:
        """
        response = requests.get(
            url=f'{URL}{path}{petId}',
            headers=HEADERS,
        )
        response.raise_for_status()
        return response

    def update_pet(self, data, path="/v2/pet"):
        """
        :param data:
        :param path:
        :return:
        """
        response = requests.put(
            url=f'{URL}{path}',
            headers=HEADERS,
            json=data
        )
        response.raise_for_status()
        return response

    def delete_pet(self, pet_id, path="/v2/pet/"):
        """
        :param pet_id:
        :param path:
        :return:
        """
        response = requests.delete(url=f'{URL}{path}{pet_id}',
                                   headers=HEADERS)
        response.raise_for_status()
        return response
