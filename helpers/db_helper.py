import psycopg2


class DbHelper:
    def __init__(self):
        self.conn = psycopg2.connect(database="postgres",
                                     user="postgres",
                                     host="localhost",
                                     password="postgres")

    def get_all_rows(self, query, table_name):
        # req = "SELECT datname FROM pg_database;"
        req = f"SELECT {query} FROM {table_name};"
        curs = self.conn.cursor()
        curs.execute(req)
        db = curs.fetchall()
        return db

    def insert_values_in_db(self, req):
        curs = self.conn.cursor()
        curs.execute(req)
        self.conn.commit()

    def delete_value_from_table(self, table_name, condition):
        req = f"DELETE FROM {table_name} WHERE {condition}"
        curs = self.conn.cursor()
        curs.execute(req)
        self.conn.commit()

    def close(self):
        self.conn.close()


s1 = DbHelper()
s1.get_all_rows("*", "auth_user")
s1.close()
