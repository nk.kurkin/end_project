import pytest
import allure
from helpers.api_helper import ApiClient


@allure.title('Create user')
@pytest.mark.parametrize("username, expected", [
    ('test_nk', 200)])
def test_create_user(username, expected):
    data = {
        "id": 9999,
        "username": username,
        "firstName": "nick",
        "lastName": "nick",
        "email": "example_tms@tms.ru",
        "password": "qwerty",
        "phone": "+09009009090",
        "userStatus": 0
    }
    create_user = ApiClient().create_user(data)
    print(create_user.text)
    assert create_user.status_code == expected


@allure.title('Log in user')
@pytest.mark.parametrize("username, password, expected", [
    ('test_nk', 'qwerty', 200)])
def test_log_in_and_get_data(username, password, expected):
    param = {
        "username": username,
        "password": password
    }
    log_in_user = ApiClient().log_in_user(param)
    assert log_in_user.status_code == expected


@allure.title('Get user data')
@pytest.mark.parametrize("username, expected", [
    ('test_nk', 200),
    pytest.param('hren_s_bugra', 404, marks=pytest.mark.xfail)])
def test_get_user_data(username, expected):
    get_data = ApiClient().get_user_data(username)
    assert get_data.status_code == expected


@allure.title('Get user data')
def test_logout_user():
    logout = ApiClient().logout_user()
    assert logout.status_code == 200


@allure.title('Delete user')
@pytest.mark.parametrize("username, expected", [
    ('test_nk', 200)])
def test_delete_user(username, expected):
    delete_user = ApiClient().delete_user(username)
    assert delete_user.status_code == expected
