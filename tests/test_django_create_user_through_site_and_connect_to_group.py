from pages.admin_page import AdminPage
from pages.main_page import MainPage
from helpers.db_helper import DbHelper
import allure


@allure.title('Login on site')
def test_login_by_admin(browser):
    with allure.step('Open page'):
        page = MainPage(browser)
        page.open_page()
    with allure.step('Open login page'):
        page.open_login_page()
    with allure.step('Authorization by admin'):
        page.authorization_by_admin()
    assert page.is_success_login(), "Huston, we have a problem with authorization"


@allure.title('Create a new user')
def test_create_user_by_admin(browser):
    with allure.step('Open creating user page'):
        page = AdminPage(browser)
        page.open_creating_user_page()
    with allure.step('Register a new user'):
        page.register_new_user_and_move_to_group()
    assert page.is_success_registration(), "something wrong"


@allure.title('Check users group in DB')
@allure.story('example story')
def test_check_db():
    with allure.step('Connect to DB and check user created'):
        db = DbHelper()
        user_table = db.get_all_rows('*', "auth_user WHERE username = 'virtuale7'")
        user_id = user_table[0][0]
    with allure.step('Check connect with user and group in DB'):
        group_table = db.get_all_rows('*', f"auth_user_groups WHERE user_id = '{user_id}'")
    assert group_table[0][2], 'User does not connect with any group'
