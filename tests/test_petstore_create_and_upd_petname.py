import pytest
import allure
from helpers.api_helper import ApiClient


@allure.title('Create pet')
@pytest.mark.parametrize("name, expected", [
    ('test_nk', 200)])
def test_create_pet(name, expected):
    data = {
        "id": 1864,
        "category": {
            "id": 0,
            "name": "string"
        },
        "name": name,
        "photoUrls": [
            "string"
        ],
        "tags": [
            {
                "id": 0,
                "name": "string"
            }
        ],
        "status": "available"
    }
    create_pet = ApiClient().create_pet(data)

    print(create_pet.text)
    assert create_pet.status_code == expected


@allure.title('Get pet info')
@pytest.mark.parametrize("pet_id, pet_name, expected", [
    (1864, 'test_nk', 200)])
def test_get_pet_info(pet_id, pet_name, expected):
    get_pet_info = ApiClient().get_by_id(pet_id)
    print(get_pet_info.text)
    assert get_pet_info.status_code == expected
    assert get_pet_info.json()['name'] == pet_name


@allure.title('Update pet name')
@pytest.mark.parametrize("pet_name, expected", [
    ('new_test_nk', 200)])
def test_update_pet_info(pet_name, expected):
    data = {
        "id": 1864,
        "category": {
            "id": 0,
            "name": "string"
        },
        "name": pet_name,
        "photoUrls": [
            "string"
        ],
        "tags": [
            {
                "id": 0,
                "name": "string"
            }
        ],
        "status": "available"
    }
    update_name = ApiClient().update_pet(data)
    print(update_name.text)
    assert update_name.status_code == expected
    assert update_name.json()['name'] == pet_name


@allure.title('Delete pet')
@pytest.mark.parametrize("pet_id, expected", [
    (1864, 200)])
def test_delete_pet(pet_id, expected):
    delete_pet = ApiClient().delete_pet(pet_id)
    assert delete_pet.status_code == expected
