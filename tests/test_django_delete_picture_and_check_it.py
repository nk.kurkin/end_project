from pages.admin_page import AdminPage
from pages.main_page import MainPage
import allure


@allure.title('Authorization by admin')
def test_login_by_admin(browser):
    with allure.step('Open main  page '):
        page = MainPage(browser)
        page.open_page()
    with allure.step('Check picture is available'):
        assert page.is_picture_exist()
    with allure.step('Open login page'):
        page.open_login_page()
    with allure.step('Authorization by admin'):
        page.authorization_by_admin()
    assert page.is_success_login(), "Huston, we have a problem with authorization"


@allure.title('Find and delete first picture')
def test_delete_first_picture(browser):
    with allure.step('Open page with picture edit'):
        page = AdminPage(browser)
        page.open_picture_edit()
    assert page.is_success_delete_picture(), "smth wrong"
