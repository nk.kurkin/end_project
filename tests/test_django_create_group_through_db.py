import allure
from helpers.db_helper import DbHelper
from pages.admin_page import AdminPage
from pages.main_page import MainPage


@allure.title('Check created user in DB')
def test_insert_new_group_into_db():
    with allure.step('Connect to DB and insert new group'):
        db = DbHelper()
        db.insert_values_in_db("insert into auth_group(name) values('test_group2');")
        new_row = db.get_all_rows('*', "auth_group where name = 'test_group2'")
    assert new_row[0][1] == 'test_group2'


@allure.title('Authorization by admin')
def test_login_and_check(browser):
    with allure.step('Open main page '):
        page = MainPage(browser)
        page.open_page()
    with allure.step('Open login page'):
        page.open_login_page()
    with allure.step('Authorization by admin'):
        page.authorization_by_admin()
    assert page.is_success_login(), "Huston, we have a problem with authorization"


@allure.title('Check new group')
def test_check_new_group_into_site(browser):
    with allure.step('Open admin page'):
        page = AdminPage(browser)
        page.open_page()
    with allure.step('Open group list'):
        page.open_group_list_page()
    with allure.step('Check our group'):
        assert page.is_group_exist(), "group does not exist"
