from pages.admin_page import AdminPage
from pages.main_page import MainPage
from helpers.db_helper import DbHelper
import allure


@allure.title('Authorization by admin')
def test_login_by_admin(browser):
    with allure.step('Open main page'):
        page = MainPage(browser)
        page.open_page()
    with allure.step('Open login page'):
        page.open_login_page()
    with allure.step('Authorization by admin'):
        page.authorization_by_admin()
    assert "Site administration" in page.get_title(), "wrong title"
    assert page.is_success_login(), "Huston, we have a problem with authorization"


@allure.title('Create a new user')
def test_create_user_by_admin(browser):
    with allure.step('Open creating user page'):
        page = AdminPage(browser)
        page.open_creating_user_page()
    with allure.step('Register a new user'):
        page.register_new_user()
    assert page.is_success_registration(), "something wrong"


@allure.title('Check created user in DB')
def test_check_db():
    with allure.step('Connect to DB and check user exist'):
        db = DbHelper().get_all_rows('*', "auth_user WHERE username = 'virtuale5'")
        assert db[0][4] == "virtuale5"


@allure.title('Logout by admin')
def test_logout_by_admin(browser):
    with allure.step('Logout'):
        page = AdminPage(browser)
        page.logout()
    assert "Logged out" in page.get_title()


@allure.title('Login by the new user')
def test_login_by_the_new_user(browser):
    with allure.step('Open main page'):
        page = MainPage(browser)
        page.open_page()
    with allure.step('Open login page'):
        page.open_login_page()
    with allure.step('Authorization by new user'):
        page.authorization_by_new_user()
    assert page.is_success_login(), "Something wrong"
