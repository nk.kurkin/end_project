from selenium.webdriver.common.by import By


class MainPageLocators:
    LOGIN_PAGE_BUTTON = (By.XPATH, '//a[@href="/admin"]')
    LOGIN_INPUT = (By.ID, 'id_username')
    PASSWORD_INPUT = (By.ID, 'id_password')
    USERNAME_ADMIN = 'admin'
    PASSWORD_ADMIN = 'password'
    USERNAME_USER = 'virtuale5'
    PASSWORD_USER = 'GFHFI>N123gfhfi.n'
    LOG_IN_BUTTON = (By.XPATH, '//input[@type="submit"]')
    PICTURE = (By.CSS_SELECTOR, "div:nth-child(10) > div > img")
