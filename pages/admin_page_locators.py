from selenium.webdriver.common.by import By


class AdminPageLocators:
    ADD_USER_LINK = (By.XPATH, '//a[@href="/admin/auth/user/add/"]')
    REGISTER_LOGIN_INPUT = (By.ID, 'id_username')
    REGISTER_PASSWORD_INPUT = (By.ID, 'id_password1')
    REGISTER_CONFIRM_PASS_INPUT = (By.ID, 'id_password2')
    REGISTER_BUTTON = (By.XPATH, '//input[@name="_save"]')
    NEEDED_CHECKBOX = (By.XPATH, '//input[@name="is_staff"]')
    SUCCESS_LOGIN_MESSAGE = (By.CSS_SELECTOR, '.colMS > h1')
    SUCCESS_REG_MESSAGE = (By.CLASS_NAME, 'success')
    NEW_USER_LOGIN = 'virtuale5'
    NEW_PASSWORD = 'GFHFI>N123gfhfi.n'
    NEW_CONFIRM_PASSWORD = 'GFHFI>N123gfhfi.n'
    LOGOUT_LINK = (By.XPATH, '//a[@href="/admin/logout/"]')
    GROUP_PAGE_LINK = (By.XPATH, '//a[@href="/admin/auth/group/"]')
    ADDED_GROUP_NAME = (By.CSS_SELECTOR, '.field-__str__ a')
    NEW_USER_F_GROUP = 'virtuale7'
    NEW_PASSWORD_F_GROUP = 'GFHFI>N123gfhfi.n'
    NEW_CONFIRM_PASSWORD_F_GROUP = 'GFHFI>N123gfhfi.n'
    SELECT_GROUP = (By.XPATH, "//select/option[@title='test_group2']")
    ADD_GROUP_TO_USER = (By.ID, "id_groups_add_link")
    EDIT_PICTURE_LINK = (By.XPATH, "//th/a[@href='/admin/app/post/']")
    FIRST_PICTURE_LINK = (By.CSS_SELECTOR, "#result_list > tbody > tr:nth-child(5) > th > a")
    DELETE_PIC_BUTTON = (By.CLASS_NAME, "deletelink")
    SURE_DELETE_PIC_BUTTON = (By.XPATH, "//div/input[@type='submit']")
    SUCCESS_MESSAGE_DELETE_PICTURE = (By.CLASS_NAME, "success")
