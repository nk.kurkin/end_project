from .admin_page_locators import AdminPageLocators
from .base_page import BasePage
from .main_page_locators import MainPageLocators


class MainPage(BasePage):
    URL = "http://localhost:8000/"

    def __init__(self, browser):
        super().__init__(browser, self.URL)

    def open_login_page(self):
        login_page = self.browser. \
            find_element(*MainPageLocators.LOGIN_PAGE_BUTTON)
        login_page.click()

    def get_title(self):
        return self.browser.title

    def authorization_by_admin(self):
        self.browser.find_element(*MainPageLocators.LOGIN_INPUT).send_keys(*MainPageLocators.USERNAME_ADMIN)
        self.browser.find_element(*MainPageLocators.PASSWORD_INPUT).send_keys(*MainPageLocators.PASSWORD_ADMIN)
        self.browser.find_element(*MainPageLocators.LOG_IN_BUTTON).click()

    def authorization_by_new_user(self):
        self.browser.find_element(*MainPageLocators.LOGIN_INPUT).send_keys(*MainPageLocators.USERNAME_USER)
        self.browser.find_element(*MainPageLocators.PASSWORD_INPUT).send_keys(*MainPageLocators.PASSWORD_USER)
        self.browser.find_element(*MainPageLocators.LOG_IN_BUTTON).click()

    def is_success_login(self):
        if self.browser.find_element(*AdminPageLocators.SUCCESS_LOGIN_MESSAGE).text == "Site administration":
            return True
        return False

    def is_picture_exist(self):
        if self.browser.find_element(*MainPageLocators.PICTURE):
            return True
        return False
