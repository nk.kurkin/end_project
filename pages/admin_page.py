from .admin_page_locators import AdminPageLocators
from .base_page import BasePage


class AdminPage(BasePage):
    URL = 'http://localhost:8000/admin/'

    def __init__(self, browser):
        super().__init__(browser, self.URL)

    def get_title(self):
        return self.browser.title

    def open_creating_user_page(self):
        self.browser.find_element(*AdminPageLocators.ADD_USER_LINK).click()

    def open_group_list_page(self):
        self.browser.find_element(*AdminPageLocators.GROUP_PAGE_LINK).click()

    def register_new_user(self):
        login = self.browser.find_element(*AdminPageLocators.REGISTER_LOGIN_INPUT)
        login.send_keys(*AdminPageLocators.NEW_USER_LOGIN)
        password_1 = self.browser.find_element(*AdminPageLocators.REGISTER_PASSWORD_INPUT)
        password_1.send_keys(*AdminPageLocators.NEW_PASSWORD)
        password_confirm = self.browser.find_element(*AdminPageLocators.REGISTER_CONFIRM_PASS_INPUT)
        password_confirm.send_keys(*AdminPageLocators.NEW_CONFIRM_PASSWORD)
        submit_button = self.browser.find_element(*AdminPageLocators.REGISTER_BUTTON)
        submit_button.click()
        checkbox_stuff = self.browser.find_element(*AdminPageLocators.NEEDED_CHECKBOX)
        checkbox_stuff.click()
        submit_button = self.browser.find_element(*AdminPageLocators.REGISTER_BUTTON)
        submit_button.click()

    def register_new_user_and_move_to_group(self):
        self.browser.find_element(*AdminPageLocators.REGISTER_LOGIN_INPUT).send_keys(*AdminPageLocators.NEW_USER_F_GROUP)
        self.browser.find_element(*AdminPageLocators.REGISTER_PASSWORD_INPUT).send_keys(*AdminPageLocators.NEW_PASSWORD_F_GROUP)
        self.browser.find_element(*AdminPageLocators.REGISTER_CONFIRM_PASS_INPUT).send_keys(*AdminPageLocators.NEW_CONFIRM_PASSWORD_F_GROUP)
        self.browser.find_element(*AdminPageLocators.REGISTER_BUTTON).click()
        self.browser.find_element(*AdminPageLocators.SELECT_GROUP).click()
        self.browser.find_element(*AdminPageLocators.ADD_GROUP_TO_USER).click()
        self.browser.find_element(*AdminPageLocators.REGISTER_BUTTON).click()

    def open_picture_edit(self):
        self.browser.find_element(*AdminPageLocators.EDIT_PICTURE_LINK).click()
        self.browser.find_element(*AdminPageLocators.FIRST_PICTURE_LINK).click()
        self.browser.find_element(*AdminPageLocators.DELETE_PIC_BUTTON).click()
        self.browser.find_element(*AdminPageLocators.SURE_DELETE_PIC_BUTTON).click()

    def is_success_delete_picture(self):
        if self.browser.find_element(*AdminPageLocators.SUCCESS_MESSAGE_DELETE_PICTURE):
            return True
        return False

    def is_group_exist(self):
        if self.browser.find_element(*AdminPageLocators.ADDED_GROUP_NAME).text == "test_group2":
            return True
        return False

    def is_success_registration(self):
        if "was changed successfully" in self.browser.find_element(*AdminPageLocators.SUCCESS_REG_MESSAGE).text:
            return True
        return False

    def logout(self):
        self.browser.find_element(*AdminPageLocators.LOGOUT_LINK).click()
